import "package:flutter/material.dart";
import 'package:flutter_app/app_screens/first_screen.dart';

void main() => runApp(MyFluterApp());


class MyFluterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "My Flutter App",
        home: Scaffold(
            appBar: AppBar(title: Text("My first App Screen")),
            body: FirstScreen()
        )
    );
  }

}